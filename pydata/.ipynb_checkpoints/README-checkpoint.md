# Tweakers workshop 2020-09-22

## Introduction

In the sections below you find the *raw* links to use when importing a notebook from URL in the IBM Cloud environment. From within your project, click **Add to project**, **Notebook**, choose the tab **from URL**. Provide a name, paste one of the URLs below and click **Create**.

Best practice is to always run the full notebook first.

From the text menu, click **Cell** and then **Run all**. Any installs will only be run once, that can take a while the first time. After running all cells you can look through the output and run individual cells with changed content by **Cell**, **Run cells** in the menu, click **Run** in the toolbar or press **Shift-Enter** when inside the cell.

If you change the code in a cell after running them all, for example in `zzcorwav.ipynb` changing the country code from `NL` to `BE`, you don't have to run all the cells above the current one. Just choose **Cell**, **Run all below** and check the results. If you are exploring data in `EUCDC.ipynb` you can just execute a cell to see the output after you have run them all.

I use two letter ISO codes for countries all over the place, here's a list:

https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2

Talking ISO, there's a problem with Greece, officially `GR` but EU CDC uses `EL`, so use the latter if you want to see their data. Not to mention problems with Namibia, country code `NA`.

## Jupyter notebooks to explore

Several notebooks are provided, you probably can't explore them all in one hour, but you can always come back for more!

If you want to dive straight in, you can start with `zzcorggd.ipynb` to generate an projections file for `zzcoradprd.ipynb` to plot a map. Just run the two notebooks after each other and see what you get!

### Getting to know the data

https://gitlab.com/dzwietering/corona/-/raw/master/tweakers/EUCDC.ipynb

This notebook shows many examples of using Pandas dataframes to organize your data and interact with it. Have a look at the visualizations, change the code to show different countries and explore the EU CDC data.

Skip this notebook if you already know Pandas or if you just want the exciting stuff!

### Clustering case data in waves

https://gitlab.com/dzwietering/corona/-/raw/master/tweakers/zzcorwav.ipynb

This is where you find my invention, don't try to understand it all at once but get a feeling of what it does by trying different countries and perhaps even tweak some selections or parameters. Can you spot the difference between The Netherlands and Belgium? The overal graphs look very similar, but things look different once you see the individual outbreaks.

### Generating local projections

https://gitlab.com/dzwietering/corona/-/raw/master/tweakers/zzcorggd.ipynb

Using individual case data from RIVM you can run the analysis per GGD region. See how this data is not aggregated per day but contains one line per case, so we do the aggregation ourselves.
Just start by running all cells (as always) and check the output in text and graphs. Note that, depending on how your environment buffers output data, it can take a couple of minutes to run before you see any output. This notebook produces an output file after a `melt` of the pivoted dataframe, so we get a result file similar to the original input, in this case containing projected data.

### Mapping projections

https://gitlab.com/dzwietering/corona/-/raw/master/tweakers/zzcoradprd.ipynb

If you ran `zzcorggd.ipynb` in the section above you have generated a file with locally projected data, so run that notebook first. We read the generated file here and use Folium to put it on a map. Lots of data manipulation in this notebook to generate the correct input for the `HeatMapWithTime` plugin that we use to generate an animated map. Have a look at the parameters for the map and see how `radius` and `use_local_extrema` influence the display. Just change the code and run the cell to check the output.

Folium documentation, specifically for `HeatMapWithTime`:

https://python-visualization.github.io/folium/plugins.html

### Conspiracy, anyone?

https://gitlab.com/dzwietering/corona/-/raw/master/tweakers/zzbenford.ipynb

There's a little known rule called Benford's law that describes just about any naturally occurring phenomenon. In other words, if a dataset doesn't follow the rule, it's probably manipulated. In this small notebook we test the EU CDC data against Benford and find that any conspiracy would have to be very sophisticated and worldwide coordinated, because it applies to this dataset at both the global and continent level. Have a look for yourself, and perhaps find out that the country level data may seem manipulated, but don't worry, it's not supposed to follow the rule.

More about Benford's law:

https://en.wikipedia.org/wiki/Benford%27s_law
