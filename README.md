# Exploring, modeling & visualizing reported case data

I'm not an epidemiologist or a virologist and just exploring data, so I will not draw conclusions on that level nor base behavior or policy upon these explorations and neither should you. Even within a country the data is heavily biased, not in the least because of testing and measurement policies changing over time!

## Workshop

The [pydata](https://gitlab.com/dzwietering/corona/-/tree/master/pydata) folder contains an up to date overview of material you can run, with workshop and additional suggestions.

## Related repositories

[Dutch alert radar by municipality](https://gitlab.com/dzwietering/coralert) using RIVM data

[World alert radar by country](https://gitlab.com/dzwietering/coralwmc) using OWID data

[Positive testing radars](https://gitlab.com/dzwietering/coradar) for World, USA, Benelux and The Netherlands


## Useful links

If you need a runtime environment use this access link:

[IBM Cloud access](https://cloud.ibm.com/)

Set up your environment this way:

[IBM Cloud setup](https://github.com/IBM/skillsnetwork/wiki/Watson-Studio-Setup)

Try this example of a pipeline implementation:

[Elyra graphical pipelines](https://developer.ibm.com/technologies/data-science/blogs/open-source-jupyter-notebooks-analyze-covid-19-data/)
